using System;
using System.Collections.Generic;
using System.Linq;
using web_api.db.InMemory;
using web_api.Helpers.CSV;
using web_api.Models;

namespace web_api.Helpers.SeedDb
{
    public static class SeedDb
    {
        public static void Context(InMemoryContext context, string path)
        {
            //string path = Path.Combine(Environment.CurrentDirectory, @"resource\", "movielist.csv");
            var records = CSVReader.ReadFile(path);

            foreach (var record in records)
            {
                var movie = new Movie();
                movie.Title = record.title;
                movie.ReleaseDate = new DateTime(int.Parse(record.year), 1, 1);
                movie.isWinner = string.IsNullOrWhiteSpace(record.winner) ? false : true;

                var studios = record.studios.Split(',');

                foreach (var st in studios)
                {
                    var studio = context.Studios.FirstOrDefault(s => s.Name == st);

                    if (studio == null)
                    {
                        studio = new Studio()
                        {
                            Name = st,
                            MovieStudios = new List<MovieStudio>()
                        };
                    }

                    studio.MovieStudios.Add(new MovieStudio()
                    {
                        MovieId = movie.Id,
                        Movie = movie,
                        StudioId = studio.Id,
                        Studio = studio
                    });

                    if (context.Studios.Contains(studio))
                        context.Studios.Update(studio);
                    else
                        context.Studios.Add(studio);
                }

                var producers = record.producers.Replace(" and ", ",").Split(',');

                foreach (var pd in producers)
                {
                    var producer = context.Producers.FirstOrDefault(p => p.Name == pd);

                    if (producer == null)
                    {
                        producer = new Producer()
                        {
                            Name = pd,
                            MovieProducers = new List<MovieProducer>()
                        };
                    }

                    producer.MovieProducers.Add(new MovieProducer()
                    {
                        MovieId = movie.Id,
                        Movie = movie,
                        ProducerId = producer.Id,
                        Producer = producer
                    });

                    if (context.Producers.Contains(producer))
                        context.Producers.Update(producer);
                    else
                        context.Producers.Add(producer);
                }

                context.Movies.Add(movie);
            }

            context.SaveChanges();
        }
    }
}
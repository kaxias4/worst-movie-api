using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;

namespace web_api.Helpers.CSV
{
    public class CSVReader
    {
        public static List<MovieCSVModel> ReadFile(string path)
        {
            using (var reader = new StreamReader(path))
            using (var csv = new CsvReader(reader))
            {
                var records = csv.GetRecords<MovieCSVModel>();
                return records.ToList();
            }
        }
    }
}
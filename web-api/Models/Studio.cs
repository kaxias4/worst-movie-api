using System;
using System.Collections.Generic;

namespace web_api.Models
{
    public class Studio
    {
        public Studio()
        {
            this.Id = new Guid();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public ICollection<MovieStudio> MovieStudios { get; set; }
    }
}
using System;
using System.Collections.Generic;

namespace web_api.Models
{
    public class Producer
    {
        public Producer()
        {
            this.Id = new Guid();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public ICollection<MovieProducer> MovieProducers { get; set; }

    }
}